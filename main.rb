# typed: ignore

require 'sinatra'

require_relative "fetching/conversion_table"
require_relative "conversion/currency_converter"

get '/convert' do
  unless params.key?("value") && params.key?("from") && params.key?("to")
    return 400
  end

  begin
    c = CurrencyConverter.new(ConversionTable.new)

    return 200, (c.convert params["value"].to_i, from: params["from"], to: params["to"]).to_s
  rescue SocketError
    return 500, "Unable to establish connection to the Internet."
  rescue StandardError => err
    return 500, err.to_s
  end
end
