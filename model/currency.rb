# typed: strict

class Currency < T::Struct
  prop :unit, Integer
  prop :czk_amount, Float
end
