# typed: true

require_relative "../fetching/conversion_table"
require "sorbet-runtime"

class CurrencyConverter
  CZK = "CZK"
  extend T::Sig

  sig { params(conversion_table: ConversionTable).void }
  def initialize(conversion_table)
    @conversion_table = conversion_table
  end

  sig { params(value: T.any(Integer, Float), options: { from: String, to: String }).returns(T.any(Float, Integer)) }
  def convert(value, options)
    unless options.key?(:from) && options.key?(:to)
      raise ConversionError, "Options of CurrencyConverter#convert must contain :from and :to keys."
    end

    if options[:from] == options[:to]
      value
    elsif options[:to] == CZK
      to_czk(value.to_f, options[:from])
    elsif options[:from] == CZK
      from_czk(value.to_f, options[:to])
    else
      from_czk(to_czk(value.to_f, options[:from]), options[:to])
    end
  end

  protected

  sig { params(value: Float, input_currency: String).returns(Float) }
  def to_czk(value, input_currency)
    curr = @conversion_table.get_currency(input_currency)

    (value / curr.unit) * curr.czk_amount
  end

  sig { params(czk_value: Float, output_currency: String).returns(Float) }
  def from_czk(czk_value, output_currency)
    curr = @conversion_table.get_currency(output_currency)

    ((czk_value * curr.unit) / curr.czk_amount)
  end
end
