# MI-RUB úkol #7: Převaděč měn
Jedná se o jednoduchý převaděč měn, který převádí na základě kurzovního lístku ČNB ([dostupný zde](https://www.cnb.cz/cs/financni-trhy/devizovy-trh/kurzy-devizoveho-trhu/kurzy-devizoveho-trhu/)).

## Technické řešení
Technické řešení využívá vlastní implementace převodu měn, micro HTTP frameworku Sinatra a **runtime type checkeru Sorbet**, který chtěl autor vyzkoušet na základě zvyků z ostatních jím používaných jazyků.

## Spuštění
1. Proveďte `git clone` tohoto repozitáře a přejděte do Vaší lokální kopie
2. `bundle install`
3. `ruby ./main.rb`

## Převody
Po spuštění HTTP serveru lze převody měn vyvolávat stylem *RPC* volání, např. volání:
```
GET http://localhost:4567/convert?value=80&from=HUF&to=CZK
```
provede převod 80 Forintů na Kč.
