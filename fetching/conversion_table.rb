# typed: true

require "net/http"
require "sorbet-runtime"

require_relative "../model/currency"
require_relative "../errors/conversion_error"

class ConversionTable
  extend T::Sig

  sig { void }
  def initialize
    uri = URI("https://www.cnb.cz/cs/financni-trhy/devizovy-trh/kurzy-devizoveho-trhu/kurzy-devizoveho-trhu/denni_kurz.txt")
    data = Net::HTTP.get(uri)

    rows = data.split("\n")
    rows.delete_at(0)
    rows.delete_at(0)

    parsed_table = rows.map { |r| r.split("|") }

    @conversion_table = {}

    parsed_table.each do |currency|
      @conversion_table[currency[3]] = Currency.new(
        unit: currency[2].to_i,
        czk_amount: currency[4].sub(",", ".").to_f
      )
    end
  end

  sig { params(code: String).returns(T.any(Currency, NilClass)) }
  def get_currency(code)
    unless @conversion_table.key? code
      raise ConversionError, "Currency Converter does not support \"#{code}\" currency."
    end

    @conversion_table[code]
  end
end
